package icom;

import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableDynamoDBRepositories(basePackages = "icom")
@SpringBootApplication
public class DynamoDbSpringApp {

    public static void main(String[] args) {
        SpringApplication.run(DynamoDbSpringApp.class, args);
    }
}

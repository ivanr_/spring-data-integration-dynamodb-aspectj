package icom.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    private static final String PATTERN = "yyyy-MM-dd";

    private static final String PATTERN_WITH_TIME = "yyyy-MM-dd HH:mm:ss";

    public static Date convertDateFromString(String stringDate, boolean dateWithTime) {
        try {
            String currentPattern = dateWithTime ? PATTERN_WITH_TIME : PATTERN;
            return getSimpleDateFormat(currentPattern).parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date(0);
        }
    }

    public static String getFormattedLogDateTime(Date date) {
        return getSimpleDateFormat(PATTERN_WITH_TIME).format(date);
    }

    private static SimpleDateFormat getSimpleDateFormat(String pattern) {
        return new SimpleDateFormat(pattern);
    }
}

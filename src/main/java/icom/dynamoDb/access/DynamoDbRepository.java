package icom.dynamoDb.access;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Comparator;

@Repository
public class DynamoDbRepository {

    @Autowired
    private AmazonDynamoDB dynamoDbClient;


    public Integer getMaxId() {
        ScanResult logsResult = dynamoDbClient.scan(new ScanRequest().withTableName("logs"));
        return logsResult.getItems().stream()
                .map(map -> map.get("id"))
                .map(attributeValue -> Integer.parseInt(attributeValue.getN()))
                .max(Comparator.naturalOrder())
                .orElseThrow(() -> new IndexOutOfBoundsException("table didn't provide ids"));
    }
}

package icom.spring.repository;

import icom.dynamoDb.entity.LogTable;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@EnableScan
public interface LogsRepository extends CrudRepository<LogTable, Integer> {
}

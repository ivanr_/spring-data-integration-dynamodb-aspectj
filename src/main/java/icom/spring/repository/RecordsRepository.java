package icom.spring.repository;

import icom.dynamoDb.entity.RecordDynamoDbTable;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

@EnableScan
public interface RecordsRepository extends CrudRepository<RecordDynamoDbTable, Integer> {
    Optional<RecordDynamoDbTable> findByRecordId(int recordId);
}

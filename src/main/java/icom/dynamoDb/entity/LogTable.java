package icom.dynamoDb.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import icom.dynamoDb.entity.converter.CreationDateTimeForLogConverter;

import java.util.Date;

@DynamoDBTable(tableName = "logs")
@DynamoDBDocument
public class LogTable {

    private Integer id;
    private String text;
    private String comment;
    private Date created;

    @DynamoDBHashKey(attributeName = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @DynamoDBAttribute(attributeName = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @DynamoDBAttribute(attributeName = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @DynamoDBAttribute(attributeName = "created")
    @DynamoDBTypeConverted(converter = CreationDateTimeForLogConverter.class)
    public Date getCreated() {
        return created;
    }

    @DynamoDBAttribute(attributeName = "created")
    public void setCreated(final Date created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "LogTable{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", comment='" + comment + '\'' +
                ", created=" + created +
                '}';
    }
}

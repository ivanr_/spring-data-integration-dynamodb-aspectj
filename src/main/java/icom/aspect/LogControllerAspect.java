package icom.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

@Aspect
@Component
@EnableAspectJAutoProxy
public class LogControllerAspect {

    @Before(value = "execution( public String getAll(..) )")
    public void logBefore(){
        System.out.println("before method accessed");
    }

    @Around(value = "execution(* icom.spring..*.*(..))")
    public Object logAround(ProceedingJoinPoint joinPoint){
        System.out.println("around method accessed");
        Object objectToReturn = null;
        try {
            objectToReturn = joinPoint.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            System.out.println("finally clause after Around method");
        }
        return objectToReturn;
    }

    @After(value = "execution(* icom..*.get*(..))")
    public void logAfter(){
        System.out.println("after method accessed");
    }

    @Pointcut(value = "execution( public String getAll(*) )")
    public void logPointCut(){
        System.out.println("asd");
    }
}

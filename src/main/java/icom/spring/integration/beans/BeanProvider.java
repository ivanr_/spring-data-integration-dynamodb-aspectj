package icom.spring.integration.beans;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.FileWritingMessageHandler;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.integration.jdbc.JdbcPollingChannelAdapter;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import javax.sql.DataSource;
import java.io.File;
import java.nio.file.Paths;

@Configuration
@EnableIntegration
public class BeanProvider {

    @Bean
    @InboundChannelAdapter(value = "inputChannel", poller = @Poller(fixedDelay = "1000"))
    public FileReadingMessageSource fileReadingMessageSource() {
        FileReadingMessageSource reader = new FileReadingMessageSource();
        reader.setDirectory(new File(Paths
                .get(".", "src", "main", "resources", "sourceFolder")
                .normalize().toAbsolutePath().toString()));
        reader.setAutoCreateDirectory(true);
        return reader;
    }

    @Bean
    @ServiceActivator(inputChannel = "inputChannel")
    public FileWritingMessageHandler fileWritingMessageHandler() {
        FileWritingMessageHandler messageHandler =
                new FileWritingMessageHandler(
                        new File(Paths
                                .get(".", "src", "main", "resources", "destinationFolder")
                                .normalize().toAbsolutePath().toString()));
        messageHandler.setAutoCreateDirectory(true);
        messageHandler.setExpectReply(false);
        return messageHandler;
    }

    @Bean
    @ServiceActivator(inputChannel = "dynamoDbChannel")
    public LoggingHandler loggingHandler() {
        LoggingHandler lh = new LoggingHandler("INFO");
        lh.setLoggerName("output-logging-from-dynamodb");
        lh.setShouldLogFullMessage(false);
        return lh;
    }

    @Bean
    @InboundChannelAdapter(value = "dynamoDbChannel", poller = @Poller(fixedDelay = "3000"))
    public MessageSource<Object> logTableMessageSource(@Autowired DataSource dataSource, @Value("${dynamodb.selectAllQuery}") String selectQuery) {
        return new JdbcPollingChannelAdapter(dataSource, selectQuery);
    }

    @Bean
    public DataSource dataSource(@Autowired AwsClientBuilder.EndpointConfiguration endpointConfiguration,
                                 @Autowired AWSCredentials credentials,
                                 @Value(value = "${dynamodb.table.region}") String tableRegion) {
        String url = String.format("jdbc:amazondynamodb:" +
                        "Region=%s;" +
                        "AWSAccessKey=%s;" +
                        "AWSSecretKey=%s",
                tableRegion,
                credentials.getAWSAccessKeyId(),
                credentials.getAWSSecretKey());
        return DataSourceBuilder.create()
                .url(url)
                .username(credentials.getAWSAccessKeyId())
                .password(credentials.getAWSSecretKey())
                .type(SingleConnectionDataSource.class)
//                add cdata jar file into the library to get driver class
                .driverClassName("cdata.jdbc.amazondynamodb.AmazonDynamoDBDriver")
                .build();
    }
}

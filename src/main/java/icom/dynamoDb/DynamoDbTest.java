package icom.dynamoDb;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ListTablesResult;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.util.IOUtils;
import icom.dynamoDb.access.DynamoDbCreds;
import icom.dynamoDb.entity.RecordDynamoDbTable;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

@Configuration
public class DynamoDbTest {

    public static final String tableName = "demo_record_table";

    public static void main(String[] args) throws IOException {
        DynamoDbCreds credentials = getCredentials();
        AmazonDynamoDB dynamoDbClient = AmazonDynamoDBClientBuilder.standard()
                .withEndpointConfiguration(new AwsClientBuilder
                        .EndpointConfiguration(credentials.getEndpoint(), credentials.getRegion()))
                .withCredentials(
                        new AWSStaticCredentialsProvider(
                                new BasicAWSCredentials(credentials.getAccessKey(), credentials.getSecretKey())))
                .build();
        ListTablesResult listTablesResult = dynamoDbClient.listTables();
        System.out.println("DynamoDb tables:\n" + listTablesResult);

        scanItems(dynamoDbClient);

        queryItem(dynamoDbClient);

        DynamoDBMapper dynamoDBMapper = new DynamoDBMapper(dynamoDbClient);
        RecordDynamoDbTable load = dynamoDBMapper.load(RecordDynamoDbTable.class, 4);
        System.out.println(load);
    }

    private static void queryItem(AmazonDynamoDB dynamoDbClient) {
        DynamoDB dynamoDB = new DynamoDB(dynamoDbClient);
        Table demoTable = dynamoDB.getTable(tableName);
        QuerySpec query = new QuerySpec()
                .withKeyConditionExpression("record_id = :v_record_id")
                .withValueMap(new ValueMap()
                        .withNumber(":v_record_id", 1))
                .withConsistentRead(true);
        ItemCollection<QueryOutcome> queryResult = demoTable.query(query);

        System.out.println("Query result");
        queryResult.forEach(System.out::println);
    }

    private static void scanItems(AmazonDynamoDB dynamoDbClient) {
        ScanResult demo_record_table = dynamoDbClient.scan(new ScanRequest().withTableName(tableName));
        System.out.println("Scan request:\n" + demo_record_table);
    }

    private static DynamoDbCreds getCredentials() throws IOException {
        String aws_home = System.getenv("aws_home");
        if (aws_home == null) {
            System.out.println("aws credentials path not found");
            throw new NullPointerException("path is null");
        }

        File credentialFile = new File(aws_home + "\\credentials");
        FileInputStream fileInputStream;
        fileInputStream = new FileInputStream(credentialFile);
        String[] lines = IOUtils.toString(fileInputStream).split(System.lineSeparator());
        return new DynamoDbCreds(getKeyByNameFromLines(lines, "aws_access_key_id"),
                getKeyByNameFromLines(lines, "aws_secret_access_key"),
                getKeyByNameFromLines(lines, "dynamoDb_endpoint"),
                getKeyByNameFromLines(lines, "dynamoDb_region"));
    }

    private static String getKeyByNameFromLines(String[] lines, String keyName) {
        return Arrays.stream(lines)
                .filter(s -> s.startsWith(keyName + "="))
                .flatMap(s -> Arrays.stream(s.split(keyName + "=")))
                .filter(s -> !s.isEmpty())
                .findFirst().orElseThrow(() -> new NullPointerException("value mustn't be null"));
    }
}

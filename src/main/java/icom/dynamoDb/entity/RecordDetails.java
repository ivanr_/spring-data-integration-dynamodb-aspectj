package icom.dynamoDb.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import icom.util.DateUtils;

import java.util.Date;

public class RecordDetails {

    private Date recordCreationTime;

    public RecordDetails() {
    }

    public RecordDetails(String recordCreationTime) {
        setRecordCreationTime(recordCreationTime);
    }

    @DynamoDBAttribute(attributeName = "recordCreationTime")
    public Date getRecordCreationTime() {
        return recordCreationTime;
    }

    public void setRecordCreationTime(final String recordCreationTime) {
        this.recordCreationTime = DateUtils.convertDateFromString(recordCreationTime, false);
    }

    @Override
    public String toString() {
        return "RecordDetails{" +
                "recordCreationTime=" + recordCreationTime +
                '}';
    }
}

package icom.spring.controller.rest;

import icom.dynamoDb.entity.RecordDynamoDbTable;
import icom.spring.repository.RecordsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RecordController {

    @Autowired
    private RecordsRepository repository;

    @GetMapping(path = "/dynamo/items")
    public String getAllItems() {
        StringBuilder sb = new StringBuilder();
        repository.findAll().forEach(recordDynamoDbTable -> sb.append(recordDynamoDbTable.toString()).append("<br>"));
        return sb.toString();
    }

    @GetMapping(path = "dynamo/items/{id}")
    public String getRecordById(@PathVariable(name = "id") int recordId) {
        System.out.println("access done");
        return repository.findByRecordId(recordId).orElse(new RecordDynamoDbTable()).toString();
    }
}

package icom.spring.integration.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.file.FileWritingMessageHandler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
@EnableScheduling
public class ScheduledLogsSaveBean {

    @Autowired
    private MessageSource<Object> dataSource;

    @Value(value = "${dynamodb.logs.folder}")
    private String folder;

    @Scheduled(fixedDelay = 5000)
    public void saveDynamoLogsToFile() {
        File folder = new File(Paths
                .get(".", "src", "main", "resources", "destinationFolder")
                .normalize().toAbsolutePath().toString());
        FileWritingMessageHandler fileWritingMessageHandler = new FileWritingMessageHandler(folder);

        Object payload = dataSource.receive().getPayload();
        if (payload instanceof List) {
            createFile((List) payload);
        }
    }

    private void createFile(List<Object> payload) {
        List<String> lines = payload.stream()
                .map(Object::toString)
                .collect(Collectors.toList());
        try {
            Files.write(Paths.get(".", "src", "main", "resources", folder,
                    new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss")
                            .format(new Date()) +
                            ".txt"), lines, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

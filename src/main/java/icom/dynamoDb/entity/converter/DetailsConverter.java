package icom.dynamoDb.entity.converter;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import icom.dynamoDb.entity.RecordDetails;

import java.util.LinkedHashMap;
import java.util.Map;

public class DetailsConverter implements DynamoDBTypeConverter<Map<String, AttributeValue>, RecordDetails> {

    private final String recordCreationTime = "recordCreationTime";

    public DetailsConverter() {
    }

    @Override
    public Map<String, AttributeValue> convert(RecordDetails details) {
        Map<String, AttributeValue> result = new LinkedHashMap<>();
        result.put(recordCreationTime, new AttributeValue(details.getRecordCreationTime().toString()));
        return result;
    }

    @Override
    public RecordDetails unconvert(Map<String, AttributeValue> detailsMap) {
        return new RecordDetails(detailsMap.get(recordCreationTime).getS());
    }
}

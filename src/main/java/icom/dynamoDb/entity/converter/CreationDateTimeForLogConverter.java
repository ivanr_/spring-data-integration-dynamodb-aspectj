package icom.dynamoDb.entity.converter;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import icom.util.DateUtils;

import java.util.Date;

public class CreationDateTimeForLogConverter implements DynamoDBTypeConverter<String, Date> {

    @Override
    public String convert(Date date) {
        return DateUtils.getFormattedLogDateTime(date);
    }

    @Override
    public Date unconvert(String created) {
        return DateUtils.convertDateFromString(created, true);
    }
}

package icom.spring.controller.rest;

import icom.dynamoDb.access.DynamoDbRepository;
import icom.dynamoDb.entity.LogTable;
import icom.spring.repository.LogsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@RestController
public class LogController { //todo: extract service layer
    @Autowired
    private LogsRepository repository;

    @Autowired
    private DynamoDbRepository clientRepository;

    @GetMapping(path = "/dynamo/logs")
    public String getAll(HttpServletRequest request) {
        String separator = getSeparator(request);
        StringBuilder sb = new StringBuilder("search result:" + separator);
        repository.findAll().forEach(logTable -> sb.append(logTable.toString()).append(separator));
        return sb.toString();
    }

    private String getSeparator(HttpServletRequest request) {
        String separator;
        if (request.getHeader("user-agent").contains("Chrome")
                || request.getHeader("user-agent").contains("Mozilla")) {
            separator = "<br>";
        } else {
            separator = System.lineSeparator();
        }
        return separator;
    }

    @GetMapping(path = "/dynamo/logs/{id}")
    public String findById(@PathVariable(name = "id") int id) {
        System.out.println("access done");
        return repository.findById(id).orElse(new LogTable()).toString();
    }

    @GetMapping(path = "/dynamo/logs/maxId")
    public String getMaxId() {
        long startTime = System.currentTimeMillis();
        return "Max id value: " + clientRepository.getMaxId() + "<br>"
                + "Execution time: " + (System.currentTimeMillis() - startTime) + " ms";
    }

    @PostMapping(path = "/dynamo/logs/addAsText")
    public String addLogsFromPathBadExample(@RequestBody String text) {
        LogTable logTable = new LogTable();
        logTable.setText(text);
        logTable.setId(clientRepository.getMaxId() + 1);
        logTable.setCreated(new Date());
        return "Saved entity: " + repository.save(logTable).toString();
    }

    @PostMapping(path = "/dynamo/logs/addLog")
    public String addLog(@RequestBody LogTable newLog){
        newLog.setId(clientRepository.getMaxId() + 1);
        if(newLog.getCreated() == null){
            newLog.setCreated(new Date());
        }
        LogTable savedLog = repository.save(newLog);
        return "Saved the whole entity: " + savedLog.toString();
    }
}
